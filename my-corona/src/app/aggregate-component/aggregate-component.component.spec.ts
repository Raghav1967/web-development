import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AggregateComponentComponent } from './aggregate-component.component';

describe('AggregateComponentComponent', () => {
  let component: AggregateComponentComponent;
  let fixture: ComponentFixture<AggregateComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AggregateComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AggregateComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
