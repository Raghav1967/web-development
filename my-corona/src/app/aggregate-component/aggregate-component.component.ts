import { Component, OnInit } from '@angular/core';
import { AggregateServiceService } from './aggregate-service.service';
import { CountriesAggregated } from './aggregate_countries';
import {Sort} from '@angular/material/sort';


@Component({
  selector: 'app-aggregate-component',
  templateUrl: './aggregate-component.component.html',
  styleUrls: ['./aggregate-component.component.css']
})
export class AggregateComponentComponent implements OnInit {

  countries:CountriesAggregated[] = []

  displayedColumns: string[] = ['index','date', 'country', 'confirmed', 'recovered', 'deaths'];

  constructor(
    private aggService : AggregateServiceService  
  ) { }
  ngOnInit(): void {

    this.aggService.getCountries().subscribe(countries =>{

      countries.forEach(element => {

        let country = new CountriesAggregated(new Date(element['date']).toLocaleString(),element['country'], element['confirmed'],
                                                  element['recovered'], element['deaths'])
                                                  
        
      this.countries.push(country)
                                                  
      });

    });

    console.log(this.countries)

  }


}
