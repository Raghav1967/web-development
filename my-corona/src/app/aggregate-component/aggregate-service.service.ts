import { Injectable } from '@angular/core';
import { CountriesAggregated } from './aggregate_countries';
import { ApiServiceService } from '../api-service.service';
import {Observable} from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class AggregateServiceService {


  constructor(
    private appService: ApiServiceService
    ) { }


  getCountries(): Observable<CountriesAggregated[]> {

    return this.appService.getAggregateCountries()
       
  }
}
