export class CountriesAggregated{

    constructor(

    private date:string,
    private country:string,
    private confirmed:number,
    private recovered:number,
    private deaths:number
    )
    {}    
    

    public get_date(): string{
        return this.date;
    }

    public get_country(): string{
        return this.country;
    }
    public get_confirmed(): number{
        return this.confirmed;
    }
    public get_recoverd(): number{
        return this.recovered;
    }
    public get_deaths(): number{
        return this.deaths;
    }

 
}

