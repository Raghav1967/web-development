import { TestBed } from '@angular/core/testing';

import { AggregateServiceService } from './aggregate-service.service';

describe('AggregateServiceService', () => {
  let service: AggregateServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AggregateServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
