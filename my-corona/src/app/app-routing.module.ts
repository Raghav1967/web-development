import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AggregateComponentComponent } from './aggregate-component/aggregate-component.component';


const routes: Routes = [

  { path: 'aggregate', component: AggregateComponentComponent },
];



@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
