import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AggregateComponentComponent } from './aggregate-component/aggregate-component.component';

import { HttpClientModule }    from '@angular/common/http';
import { AggregateServiceService } from './aggregate-component/aggregate-service.service';
import { ApiServiceService } from './api-service.service';
import {MatTableModule} from '@angular/material/table'
import {FormsModule, ReactiveFormsModule} from '@angular/forms';


@NgModule({
  declarations: [
    AppComponent,
    AggregateComponentComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    MatTableModule,
    FormsModule,
    ReactiveFormsModule

  ],
  providers: [AggregateServiceService, ApiServiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
