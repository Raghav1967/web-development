import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AggregateServiceService } from './aggregate-component/aggregate-service.service';
import { catchError } from 'rxjs/operators';
import { CountriesAggregated } from './aggregate-component/aggregate_countries';
import {Observable, throwError} from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class ApiServiceService {

  private Url = 'http://127.0.0.1:5000/aggregate';  // URL to web api

  constructor(
    private http: HttpClient) { }


getAggregateCountries (): Observable<CountriesAggregated[]> {

  return this.http.get<CountriesAggregated[]>(this.Url)
        .pipe(
          catchError(this.handleError)
        );
}

/**
 * Handle Http operation that failed.
 * Let the app continue.
 * @param operation - name of the operation that failed
 * @param result - optional value to return as the observable result
 */
private handleError(error) {
  
  let errorMessage = '';
        if (error.error instanceof ErrorEvent) {
            // client-side error
            errorMessage = `Error: ${error.error.message}`;
        } else {
            // server-side error
            errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
        }
        console.log(errorMessage);
        return throwError(errorMessage);
    }

}