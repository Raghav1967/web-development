from preprocessing import Preprocessing
import pandas as pd

f1 = open("positive-words.txt", 'r')
f2 = open("negative-words.txt", 'r')
positive_words = []
negative_words = []
for i in f1:
    positive_words.append(i.rstrip('\n'))
for i in f2:
    negative_words.append(i.rstrip('\n').lstrip('\t'))


preprocess = Preprocessing()

count = 0
positive = 0
negative = 0

df = pd.read_csv('normalized1mill.csv')

df.dropna(inplace=True)

positive_df = pd.read_csv('positive.csv')

positive_df= positive_df.set_index('word')

text = df['text'].values

for tweet in text:

    positivewordcount = 0
    negativewordcount = 0
    score = 0
    count += 1

    normalize_text =tweet

    for i in normalize_text.split():
        if i in positive_words:
            positive_df.loc[i, 'count'] += 1

        elif i in negative_words:
            negativewordcount += 1



    print("Total no of positive words in Tweet : " + str(positivewordcount), normalize_text)
    print("Total no of negative words in Tweet : " + str(negativewordcount))

positive_df.to_csv('updated_positive.csv')