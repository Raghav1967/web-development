

class CountriesAggregated:
    def __init__(self, date, country, confirmed, recovered, deaths):

        self.date = date
        self.country = country
        self.confirmed = confirmed
        self.recovered = recovered
        self.deaths = deaths

