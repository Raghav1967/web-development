from flask import Flask, Response
import psycopg2
import pandas as pd
from flask_cors import CORS
from flask import request
from flask import jsonify
import json


app = Flask(__name__)
cors = CORS(app)

@app.route('/')
def hello_world():
    return 'Hello World!'


if __name__ == '__main__':
    app.run(host='0.0.0.0')

# Test getting data
conn_params = {
    'host': 'localhost',
    'port': '5432',
    'database': 'corona',
    'user': 'raghav',
    'password': 'raghav@123'
}

df = pd.read_csv('training.1600000.processed.noemoticon.csv', nrows=100, header=None, encoding = "ISO-8859-1", engine='python',
                 names=['output', 'time', 'query', 'user', 'text'])


class ConnectionPull:
    rows = None
    columns = None
    countries_aggregated = "select * from data.countries_aggregated"

    def __init__(self, conn_params):
        self.conn_params = conn_params

    def run_query(self, conn_params):
        conn_str = 'dbname={} user={} password={} host={}'.format(
            conn_params['database'], conn_params['user'], conn_params['password'], conn_params['host'])
        with psycopg2.connect(conn_str) as conn:
            conn.autocommit = True
            with conn.cursor() as cur:
                cur.execute(self.countries_aggregated)
                rows = cur.fetchall()
                columns = [desc[0] for desc in cur.description]
        self.rows = rows
        self.columns = columns

    def get_rows(self):
        return (self.rows)

    def get_columns(self):
        return (self.columns)

    def get_dataframe(self):
        self.run_query(self.conn_params)
        return (pd.DataFrame(self.rows, columns=self.columns))



@app.route('/aggregate', methods=['GET'])
def get_countries_aggregate():

    connection = ConnectionPull(conn_params)
    result = connection.get_dataframe()


    result = result.to_json(orient='records', date_format = "iso")


    return Response(response=result, status=200, content_type="application/json")


@app.route('/aggregateCountry/<country>', methods=['GET'])
def get_aggregate_by_country(country):

    country = request.view_args['country']

    print(country)

    connection = ConnectionPull(conn_params)
    result = connection.get_dataframe()

    country_data = result.groupby('country').get_group(country)

    country_data = country_data.to_json(orient='records', date_format="iso")

    return Response(response=country_data, status=200, content_type="application/json")



@app.route('/getAllCountries', methods=['GET'])
def get_all_countries():


    connection = ConnectionPull(conn_params)
    result = connection.get_dataframe()

    countries = result.groupby('country').count().index.tolist()

    countries = json.dumps(countries)

    return Response(response=countries, status=200, content_type="application/json")



@app.route('/getTweets', methods=['GET'])
def get_all_tweets():

    result = df

    text = result['text'].tolist()

    text = json.dumps(text)

    return Response(response=text, status=200, content_type="application/json")


@app.route('/getPositiveTweets', methods=['GET'])
def get_all_positive_tweets():



    result = pd.read_csv('updated_positive.csv')

    result = result.sort_values(by= 'count', ascending=False)

    result = result.head(10)

    text = result.to_json(orient='records')

    return Response(response=text, status=200, content_type="application/json")

# @app.route('/getNegativeTweets', methods=['GET'])
# def get_all_tweets():
#
#     result = df
#
#     text = result['text'].tolist()
#
#     text = json.dumps(text)
#
#     return Response(response=text, status=200, content_type="application/json")


@app.route("/get_my_ip", methods=["GET"])
def get_my_ip():
    return jsonify({'ip': request.remote_addr}), 200


